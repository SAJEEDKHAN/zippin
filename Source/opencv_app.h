#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include "opencv2/core/core.hpp"
#include "opencv2/opencv.hpp"
#include "opencv2/videoio/videoio.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/video.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <opencv2/imgcodecs.hpp>

#define GRAY_FRAME_CONVERSION 	1
#define TEMPLATE_MATCHING	1
#define MIN_MAX_LOC 		1
#define FRAME_RESOLUTION 	1 
#define FRAME_CAPTURE 		1
#define VIDEO_CAPTURE		1
#define MEAN_SD_CALC		1
#define STATIC_ROI		0
#define WINDOW_WIDTH		1366
#define WINDOW_HEIGHT		768
#define RELEASE_IMAGE		1
using namespace cv;

#include <iostream>
using namespace std;
float CalculateSD(uint32_t roiLeft, uint32_t roiTop, uint32_t roiWidth, uint32_t roiHeight, uint32_t iBufferWidth, uint32_t iBufferHeight, uint8_t *uiImageBuffer, float * sdvalue,  int *Pixelvariation);
float roundofFiveDecimals(float var);
