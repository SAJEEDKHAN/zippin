#include <gst/gst.h>

int
main (int argc, char *argv[])
{
  GstElement *pipeline,*pipeline1;
  GstBus *bus;
  GstMessage *msg;

  /* Initialize GStreamer */
  gst_init (&argc, &argv);
#if 0 
  pipeline = gst_parse_launch("v4l2src device=/dev/video0 ! videoconvert ! video/x-raw,format=RGB ! v4l2sink device=/dev/video3 ",NULL);
#else
  pipeline = gst_parse_launch(" rtspsrc location=rtsp://viewer:password@192.168.88.130:8554/camerastream latency=0 ! decodebin ! videoconvert ! video/x-raw,format=RGB ! videoconvert ! v4l2sink device=/dev/video3 ",NULL);
#endif 
  /* Start playing */
  gst_element_set_state (pipeline, GST_STATE_PLAYING);

  /* Wait until error or EOS */
  bus = gst_element_get_bus (pipeline);
  msg =
      gst_bus_timed_pop_filtered (bus, GST_CLOCK_TIME_NONE, GST_MESSAGE_EOS);

  /* See next tutorial for proper error message handling/parsing */
  if (GST_MESSAGE_TYPE (msg) == GST_MESSAGE_ERROR) {
    g_error ("An error occurred! Re-run with the GST_DEBUG=*:WARN environment "
        "variable set for more details.");
  }

  /* Free resources */
  gst_message_unref (msg);
  gst_object_unref (bus);
  gst_element_set_state (pipeline, GST_STATE_NULL);
  gst_object_unref (pipeline);
  return 0;
}




