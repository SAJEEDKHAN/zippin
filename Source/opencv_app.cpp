#include "opencv_app.h"

int main()
{

#if VIDEO_CAPTURE

#if 1
	VideoCapture cap(3);

#else

	VideoCapture cap("rtspsrc location=rtsp://viewer:password@192.168.88.130:8554/camerastream latency=0 ! rtph264depay ! h264parse ! avdec_h264 ! videoconvert ! appsink",CAP_GSTREAMER);

#endif
	if (!cap.isOpened()) {
		cerr <<"VideoCapture not opened"<<endl;
		exit(-1);
	}
#endif

	namedWindow( "Frame",WINDOW_NORMAL );						//Fitting to the window screen
	while(1){

#if FRAME_CAPTURE
		Mat InputFrame;

		cap >> InputFrame;

		if (InputFrame.empty())							// If the frame is empty, break immediately

		{
			cout << endl << endl << "No Frame Received! Camera is Unavailable!" << endl << endl;
			return -1;
		}
		cv::resizeWindow("Frame", WINDOW_WIDTH, WINDOW_HEIGHT);		
#endif

#if FRAME_RESOLUTION
		int row = InputFrame.rows;
		int col = InputFrame.cols;
		cout << endl << "WIDTH  : " << col << endl << "HEIGHT : " << row << endl << endl ;
#endif


#if GRAY_FRAME_CONVERSION

		Mat Grayframe, Grayframe1;

		cvtColor(InputFrame, Grayframe, cv::COLOR_RGB2GRAY); 			//color conversion RBG to GRAYSCALE
#endif

#if TEMPLATE_MATCHING
		Mat Templateframe, result1, result;

		Templateframe = cv::imread("Template.png", 0);

		matchTemplate(Grayframe, Templateframe, result1, TM_CCOEFF_NORMED);	

		normalize(result1, result, 0, 1, NORM_MINMAX, -1, Mat());

		imwrite("result.png", result);
#endif

#if MIN_MAX_LOC
		int result_cols, result_rows;
		double minVal,maxVal; 
		Point minLoc,maxLoc, matchLoc;

		result_cols = Grayframe.cols - Templateframe.cols + 1;
		result_rows = Grayframe.rows - Templateframe.rows + 1;

		minMaxLoc(result, &minVal, &maxVal, &minLoc, &maxLoc, Mat());
		matchLoc = maxLoc;

#endif


#if MEAN_SD_CALC
		float Mean,SDValue;
		int Pixel_variation;
		uint32_t roiLeft,roiTop,roiWidth,roiHeight;
#if STATIC_ROI
		roiLeft   =  10;
		roiTop    =  10;
		roiWidth  =  500;
		roiHeight =  500;
#else
#if 1
		roiLeft   =  maxLoc.x;
		roiTop    =  matchLoc.y ;
		roiWidth  =  Templateframe.cols;
		roiHeight =  Templateframe.rows;
#else
		roiLeft   =  maxLoc.x - 180;
		roiTop    =  matchLoc.y -166;
		roiWidth  =  Templateframe.cols + 355;
		roiHeight =  Templateframe.rows + 190;

#endif
#endif
		printf(" roiLeft-%d, roiTop-%d, roiWidth-%d, roiHeight-%d\n", roiLeft, roiTop, roiWidth, roiHeight);

		Mean = CalculateSD(roiLeft, roiTop, roiWidth, roiHeight, col, row, Grayframe.data, &SDValue, &Pixel_variation);
#endif
		printf("MEAN : %f SD : %f",Mean,SDValue);
		
		
		// Press  ESC on keyboard to exit
		char c=(char)waitKey(25);
		if(c==27)
			break;
	}
	// When everything done, release the video capture object
	// Closes all the frames
	destroyAllWindows();
	return 0;
}

float CalculateSD(uint32_t roiLeft, uint32_t roiTop, uint32_t roiWidth, uint32_t roiHeight, uint32_t iBufferWidth, uint32_t iBufferHeight, uint8_t *uiImageBuffer, float * sdvalue,  int *Pixelvariation)
{
	char Temp[100];
	stringstream ss;
	cv::Scalar Mean, stddev;
	double size = roiWidth * roiHeight;

	double CannyAccThreshold = 0, CannyThreshold = 0;

	cv::Mat croppedImage, EdgeImage, GaussianImage, ThreshImage;

	cv::Mat GrayImage(iBufferHeight, iBufferWidth, CV_8UC1, uiImageBuffer); 
	
	cv::Mat ROI(GrayImage, cv::Rect(roiLeft, roiTop, roiWidth, roiHeight));
	//imshow("Frame",ROI);

	// Copy the data into new matrix
	ROI.copyTo(croppedImage);

	imshow("Frame",croppedImage);

	//Gaussian Image - Blur
	GaussianBlur(croppedImage, GaussianImage, cv::Size(3, 3), 1.5, 1.5);
	
	//Thresh Image
	CannyAccThreshold = threshold(GaussianImage, ThreshImage, 0, 255, cv::THRESH_OTSU | cv::THRESH_BINARY);
	CannyThreshold = 0.1 * CannyAccThreshold;

	//Edge Image - edge detection
	Canny(GaussianImage, EdgeImage, CannyThreshold, CannyAccThreshold, 3, false);
	
	cv::Scalar Sum = sum(EdgeImage);
	float SdVal = (Sum.val[0] / size);

	//Average Edge Count
	cv::meanStdDev(EdgeImage, Mean, stddev, cv::Mat());
	*sdvalue = stddev.val[0];

#if RELEASE_IMAGE
	GrayImage.release();
	EdgeImage.release();
	GaussianImage.release();
	ThreshImage.release();		
	ROI.release();
	croppedImage.release();
#endif
	return  roundofFiveDecimals(SdVal);
}


float roundofFiveDecimals(float var)
{

	float value = (int)(var * 100000 + .5);
	return (float)value / 100000;
}
